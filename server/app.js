'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var methodOverride = require('method-override');
var config = require('./config');
var log = require('./libs/log')(module);
var webpack = require ('webpack');
var favicon = require('serve-favicon');
import configW from '../webpack.config.dev';
require('./libs/mongoose');

var groups = require('./routes/groups');
var users = require('./routes/users');
var common = require('./routes/common');

var app = express();

const compiler = webpack(configW);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: configW.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(favicon(__dirname + '/../src/img/profile.png'));
app.use(express.static(path.join(__dirname + '/../src/')));

app.use('/api/groups', groups);
app.use('/api/users', users);
app.use('/api/common', common);

// error handlers
app.use(function (err, req, res, next) {
  if (err.name === 'ValidationError') {
    err.status = 422;
  }
  log.error('%s %d %s', req.method, err.status ? err.status : req.statusCode, err.message);

  return res.status(err.status ? err.status : 500)
    .json({
      nameError: err.name,
      messageError: err.message ? err.message : 'Error',
      validationError: err.errors,
      growlMessages: [{'text': err.message, 'tittle': err.name, 'severity': 'warning'}]
    });
});

app.listen(config.get('port'), function () {
  log.info('sever run ' + config.get('port'));
});

module.exports = app;
