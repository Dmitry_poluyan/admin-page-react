'use strict';
var UserModel = require('../models/user');
var GroupModel = require('../models/group');

module.exports.userById = function (req, res, next, user_id) {
  return UserModel
    .findById(user_id)
    .then(function (user) {
      if (!user) {
        var err = new Error('Not found');
        err.status = 404;
        return next(err);
      }
      req.user = user;
      return next();
    })
    .catch(next);
};

module.exports.groupById = function (req, res, next, group_id) {
  return GroupModel
    .findById(group_id)
    .then(function (group) {
      if (!group) {
        var err = new Error('Not found');
        err.status = 404;
        return next(err);
      }
      req.group = group;
      return next();
    })
    .catch(next);
};