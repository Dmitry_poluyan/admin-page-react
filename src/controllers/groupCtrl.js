import GroupsApi from '../api/groupsApi';
import * as groupActions from '../actions/groupActions';
import {beginAjaxCall, ajaxCallError, endAjaxCall} from '../actions/ajaxStatusActions';

export function loadGroups() {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .getAllGroups()
            .then(groups => dispatch(groupActions.loadGroupsSuccess(groups)))
            .catch(error => {
                console.log(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function createGroup(group) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .createGroup(group)
            .then(res => dispatch(groupActions.createGroupSuccess(res)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function updateGroup(group) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .updateGroupById(group)
            .then(res => res.nModified > 0 ? dispatch(groupActions.updateGroupByIdSuccess(group)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function getGroupById(group) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .getGroupById(group)
            .then(res => dispatch(groupActions.getGroupByIdSuccess(res)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function getUserGroupsByUserId(userId) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .getUserGroupsByUserId(userId)
            .then(groups => dispatch(groupActions.getUserGroupsByUserIdSuccess(groups)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function deleteGroupById(id) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return GroupsApi
            .deleteGroupById(id)
            .then(res => res.n > 0 ? dispatch(groupActions.deleteGroupByIdSuccess(id)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}
