import UsersApi from '../api/usersApi';
import * as userActions from '../actions/userActions';
import {beginAjaxCall, ajaxCallError, endAjaxCall} from '../actions/ajaxStatusActions';

export function loadUsers() {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .getAllUsers()
            .then(users => dispatch(userActions.loadUsersSuccess(users)))
            .catch(error => {
                console.log(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function getUsersByPageNumber(page) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .getUsersByPageNumber(page)
            .then(res => dispatch(userActions.getUsersByPageNumberSuccess(res.users, res.count, res.size)))
            .catch(error => {
                console.log(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function createUser(user) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .createUser(user)
            .then(res => dispatch(userActions.createUserSuccess(res)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function updateUser(user) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .updateUserById(user)
            .then(res => res.nModified > 0 ? dispatch(userActions.updateUserByIdSuccess(user)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function getUserById(user) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .getUserById(user)
            .then(res => dispatch(userActions.getUserByIdSuccess(res)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function searchUsersByCharacters(characters) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .searchUsersByCharacters(characters)
            .then(users => dispatch(userActions.searchUsersByCharactersSuccess(users)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function getGroupUsersByGroupId(groupId) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .getGroupUsersByGroupId(groupId)
            .then(users => dispatch(userActions.getGroupUsersByGroupIdSuccess(users)))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function deleteUserById(id) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .deleteUserById(id)
            .then(res => res.n > 0 ? dispatch(userActions.deleteUserByIdSuccess(id)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function addGroupForUser(groupId, userId) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .addGroupForUser(groupId, userId)
            .then(res => res.nModified > 0 ? dispatch(userActions.addGroupForUserSuccess(groupId, userId)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}

export function deleteGroupFromUser(groupId, userId) {
    return dispatch => {
        dispatch(beginAjaxCall());

        return UsersApi
            .deleteGroupFromUser(groupId, userId)
            .then(res => res.nModified > 0 ? dispatch(userActions.deleteGroupFromUserSuccess(groupId, userId)) : dispatch(endAjaxCall()))
            .catch(error => {
                console.error(error);
                dispatch(ajaxCallError());
                throw(error);
            });
    };
}
