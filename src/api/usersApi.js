import axios from 'axios';

const ROOT_URL = 'http://localhost:3000/api/users';

export default class UsersApi {
    static getAllUsers() {
        return axios({
            method: 'GET',
            url: ROOT_URL
        }).then(res => res.data.users);
    }

    static getUsersByPageNumber(page) {
        page = page ? page : 1;

        return axios({
            method: 'GET',
            url: `${ROOT_URL}/usersByPageNumber/${page}`
        }).then(res => res.data);
    }

    static getUserById(id) {
        return axios({
            method: 'GET',
            url: `${ROOT_URL}/${id}`
        }).then(res => res.data.user);
    }

    static searchUsersByCharacters(characters) {
        return axios({
            method: 'GET',
            url: `${ROOT_URL}/search/${characters}`
        }).then(res => res.data.usersSearchResult);
    }

    static createUser(user) {
        return axios({
            method: 'POST',
            url: ROOT_URL,
            data: user
        }).then(res => res.data.user);
    }

    static updateUserById(user) {
        return axios({
            method: 'PUT',
            url: `${ROOT_URL}/${user._id}`,
            data: user
        }).then(res => res.data.modifiedUser);
    }

    static getGroupUsersByGroupId(id) {
        return axios({
            method: 'GET',
            url: `${ROOT_URL}/usersGroup/${id}`
        }).then(res => res.data.users);
    }

    static deleteUserById(id) {
        return axios({
            method: 'DELETE',
            url: `${ROOT_URL}/${id}`
        }).then(res => res.data.modifiedUsers);
    }

    static addGroupForUser(groupId, userId) {
        return axios({
            method: 'PUT',
            url: `${ROOT_URL}/${userId}/addGroup/${groupId}`
        }).then(res => res.data.users);
    }

    static deleteGroupFromUser(groupId, userId) {
        return axios({
            method: 'PUT',
            url: `${ROOT_URL}/${userId}/deleteGroup/${groupId}`
        }).then(res => res.data.users);
    }
}
