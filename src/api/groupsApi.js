import axios from 'axios';

const ROOT_URL = 'http://localhost:3000/api/groups';

export default class GroupsApi {
    static getAllGroups() {
        return axios({
            method: 'GET',
            url: ROOT_URL
        }).then(res => res.data.groups);
    }

    static getGroupById(id) {
        return axios({
            method: 'GET',
            url: `${ROOT_URL}/${id}`
        }).then(res => res.data.group);
    }

    static getUserGroupsByUserId(id) {
        return axios({
            method: 'GET',
            url: `${ROOT_URL}/groupsUser/${id}`
        }).then(res => res.data.groups);
    }

    static createGroup(group) {
        return axios({
            method: 'POST',
            url: ROOT_URL,
            data: group
        }).then(res => res.data.group);
    }

    static updateGroupById(group) {
        return axios({
            method: 'PUT',
            url: `${ROOT_URL}/${group._id}`,
            data: group
        }).then(res => res.data.modifiedGroup);
    }

    static deleteGroupById(id) {
        return axios({
            method: 'DELETE',
            url: `${ROOT_URL}/${id}`
        }).then(res => res.data.modifiedGroups);
    }
}
