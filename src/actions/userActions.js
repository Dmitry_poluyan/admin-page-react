import * as types from '../constants/actionTypes';

export function loadUsersSuccess(users) {
    return {
        type: types.LOAD_USERS_SUCCESS,
        users: users
    };
}

export function getUsersByPageNumberSuccess(users, totalCount, numOnPage) {
    return {
        type: types.GET_USERS_BY_PAGE_NUMBER_SUCCESS,
        users: users,
        totalCount: totalCount,
        numOnPage: numOnPage
    };
}

export function createUserSuccess(user) {
    return {
        type: types.CREATE_USER_SUCCESS,
        user: user
    };
}

export function getUserByIdSuccess(user) {
    return {
        type: types.GET_USER_BY_ID_SUCCESS,
        user: user
    };
}

export function searchUsersByCharactersSuccess(users) {
    return {
        type: types.SEARCH_USERS_BY_CHARACTERS_SUCCESS,
        users: users
    };
}

export function updateUserByIdSuccess(user) {
    return {
        type: types.UPDATE_USER_BY_ID_SUCCESS,
        user: user
    };
}

export function deleteUserByIdSuccess(id) {
    return {
        type: types.DELETE_USER_BY_ID_SUCCESS,
        id: id
    };
}

export function getGroupUsersByGroupIdSuccess(users) {
    return {
        type: types.GET_GROUP_USERS_BY_ID_SUCCESS,
        users: users
    };
}

export function addGroupForUserSuccess(groupId, userId) {
    return {
        type: types.ADD_GROUP_FOR_USER_SUCCESS,
        groupId: groupId,
        userId: userId
    };
}

export function deleteGroupFromUserSuccess(groupId, userId) {
    return {
        type: types.DELETE_GROUP_FROM_USER_SUCCESS,
        groupId: groupId,
        userId: userId
    };
}
