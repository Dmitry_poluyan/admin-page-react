import * as types from '../constants/actionTypes';

export function loadGroupsSuccess(groups) {
    return {
        type: types.LOAD_GROUPS_SUCCESS,
        groups: groups
    };
}

export function createGroupSuccess(group) {
    return {
        type: types.CREATE_GROUP_SUCCESS,
        group: group
    };
}

export function getGroupByIdSuccess(group) {
    return {
        type: types.GET_GROUP_BY_ID_SUCCESS,
        group: group
    };
}

export function getUserGroupsByUserIdSuccess(groups) {
    return {
        type: types.GET_USER_GROUPS_BY_ID_SUCCESS,
        groups: groups
    };
}

export function updateGroupByIdSuccess(group) {
    return {
        type: types.UPDATE_GROUP_BY_ID_SUCCESS,
        group: group
    };
}

export function deleteGroupByIdSuccess(id) {
    return {
        type: types.DELETE_GROUP_BY_ID_SUCCESS,
        id: id
    };
}

