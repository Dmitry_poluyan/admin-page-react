import {combineReducers} from 'redux';

import USER from './userReducer';
import GROUP from './groupReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';

const  rootReducer = combineReducers({
    USER,
    GROUP,
    ajaxCallsInProgress
});

export default rootReducer;
