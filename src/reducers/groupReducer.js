import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function groupReducer(state = initialState.GROUP, action) {
    switch (action.type) {
        case types.LOAD_GROUPS_SUCCESS: {
            return Object.assign({}, state, {groups: action.groups.map(group => Object.assign({}, group))});
        }

        case types.CREATE_GROUP_SUCCESS: {
            let newGroupsArray = state.groups.map(group => Object.assign({}, group));
            newGroupsArray.push(action.group);

            return Object.assign({}, state, {groups: newGroupsArray});
        }

        case types.GET_GROUP_BY_ID_SUCCESS: {
            return Object.assign({}, state, {currentGroup: action.group});
        }

        case types.GET_USER_GROUPS_BY_ID_SUCCESS: {
            return Object.assign({}, state, {userGroups: action.groups.map(group => Object.assign({}, group))});
        }

        case types.DELETE_GROUP_BY_ID_SUCCESS: {
            return Object.assign({}, state,
                {
                    groups: state.groups.filter(group => group._id !== action.id),
                    currentGroup: {}
                }
            );
        }

        case types.UPDATE_GROUP_BY_ID_SUCCESS: {
            return Object.assign({}, state,
                {
                    groups: state.groups.map(group => group._id === action.group._id ? Object.assign({}, action.group) : group),
                    currentGroup: Object.assign({}, action.group)
                }
            );
        }

        case types.DELETE_GROUP_FROM_USER_SUCCESS: {
            return Object.assign({}, state, {userGroups: state.userGroups.filter(group => group._id !== action.groupId)});
        }

        default: {
            return state;
        }
    }
}
