export default {
    USER: {
        users: [],
        currentUser: {},
        groupUsers: [],
        pagination: {
            totalCount: 0,
            numOnPage: 0
        }
    },
    GROUP: {
        groups: [],
        currentGroup: {},
        userGroups: []
    },
    ajaxCallsInProgress: 0
};
