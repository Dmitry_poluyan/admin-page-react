import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.USER, action) {
    switch (action.type) {
        case types.LOAD_USERS_SUCCESS: {
            return Object.assign({}, state, {users: action.users.map(user => Object.assign({}, user))});
        }

        case types.SEARCH_USERS_BY_CHARACTERS_SUCCESS: {
            return Object.assign({}, state, {users: action.users.map(user => Object.assign({}, user))});
        }

        case types.GET_USERS_BY_PAGE_NUMBER_SUCCESS: {
            return Object.assign({}, state,
                {
                    users: action.users.map(user => Object.assign({}, user)),
                    pagination: {
                        totalCount: action.totalCount,
                        numOnPage: action.numOnPage
                    }
                }
            );
        }

        case types.CREATE_USER_SUCCESS: {
            let newUsersArray = state.users.map(user => Object.assign({}, user));
            newUsersArray.push(action.user);

            return Object.assign({}, state, {users: newUsersArray});
        }

        case types.GET_USER_BY_ID_SUCCESS: {
            return Object.assign({}, state, {currentUser: action.user});
        }

        case types.DELETE_USER_BY_ID_SUCCESS: {
            return Object.assign({}, state,
                {
                    users: state.users.filter(user => user._id !== action.id),
                    currentUser: {}
                }
            );
        }

        case types.UPDATE_USER_BY_ID_SUCCESS: {
            return Object.assign({}, state,
                {
                    users: state.users.map(user => user._id === action.user._id ? Object.assign({}, action.user) : user),
                    currentUser: Object.assign({}, action.user)
                }
            );
        }

        case types.ADD_GROUP_FOR_USER_SUCCESS: {
            let newGroupIds = state.currentUser.groupId.map(id => id);
            newGroupIds.push(action.groupId);
            let newCurrentUser = Object.assign({}, state.currentUser, {groupId: newGroupIds});

            return Object.assign({}, state,
                {
                    users: state.users.map(user => user._id === action.userId ? newCurrentUser : user),
                    currentUser: newCurrentUser
                }
            );
        }

        case types.DELETE_GROUP_FROM_USER_SUCCESS: {
            let newGroupIds = state.currentUser.groupId.filter(id => id !== action.groupId);
            let newCurrentUser = Object.assign({}, state.currentUser, {groupId: newGroupIds});

            return Object.assign({}, state,
                {
                    users: state.users.map(user => user._id === action.userId ? newCurrentUser : user),
                    currentUser: newCurrentUser
                }
            );
        }

        case types.GET_GROUP_USERS_BY_ID_SUCCESS: {
            return Object.assign({}, state, {groupUsers: action.users.map(user => Object.assign({}, user))});
        }

        default: {
            return state;
        }
    }
}
