import React, {PropTypes} from 'react';
import SelectInput from '../../common/presentational/SelectInput';

const AddGroupForUserForm = ({group, groups, onSubmit, onChange, onCancel, saving}) => {
    function updateData(event) {
        let field = event.target.name;
        group[field] = event.target.value;
        onChange(group);
    }

    function submitForm(event) {
        event.preventDefault();
        onSubmit();
    }

    function goToBack() {
        window.history.back();
    }

    return (
        <form className="form-horizontal adv-margin-form-addG">

            <SelectInput
                name="_id"
                label="Group"
                onChange={updateData}
                defaultOption="Select"
                value={group._id}
                keyKey={'_id'}
                keyValue={'_id'}
                keyText={'groupName'}
                options={groups}
            />

            <div className="btn-form-right">
                <input
                    type="submit"
                    disabled={saving}
                    value={saving ? 'Adding...' : 'Add'}
                    className="btn btn-primary btn-margin"
                    onClick={submitForm}
                />

                <a disabled={saving}
                   className="btn btn-primary"
                   onClick={onCancel  ? onCancel : goToBack}
                >
                    Cancel
                </a>
            </div>
        </form>
    );
};

AddGroupForUserForm.propTypes = {
    group: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    onChange: PropTypes.func,
    saving: PropTypes.bool,
    errors: PropTypes.object
};

export default AddGroupForUserForm;
