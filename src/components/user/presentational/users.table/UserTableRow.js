import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const UserTableRow = ({user}) => {
    return (
        <tr>
            <td>
                <Link to={'/admin/users/details/' + user._id}>{user.userName}</Link>
            </td>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.email}</td>
        </tr>
    );
};

UserTableRow.propTypes = {
    user: PropTypes.object.isRequired
};

export default UserTableRow;
