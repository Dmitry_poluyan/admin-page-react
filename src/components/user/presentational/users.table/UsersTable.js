import React, {PropTypes} from 'react';
import UserTableRow from './UserTableRow';

const UsersTable = ({users}) => {
    let usersRows = users.map(user =><UserTableRow key={user._id} user={user}/>);

    return (
        <div>
            <table className="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>User name</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                {usersRows}
                </tbody>
            </table>
        </div>
    );
};

UsersTable.propTypes = {
    users: PropTypes.array.isRequired
};

export default UsersTable;
