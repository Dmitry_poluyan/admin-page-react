import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';

const UserNavbar = ({id, onDelete}) => {
    return (
        <ul className="nav nav-pills nav-stacked adm-navbar">
            <li>
                <IndexLink to={'admin/users/details/' + id + '/profile'} activeClassName="active">Profile user</IndexLink>
            </li>
            <li>
                <Link to={'admin/users/details/' + id + '/edit'} activeClassName="active">Edit user</Link>
            </li>
            <li>
                <Link to={'admin/users/details/' + id + '/groups'} activeClassName="active">Groups user</Link>
            </li>
            <li>
                <a onClick={onDelete}>Delete user</a>
            </li>
        </ul>
    );
};

UserNavbar.propTypes = {
    id: PropTypes.string.isRequired,
    onDelete: PropTypes.func.isRequired
};

export default UserNavbar;
