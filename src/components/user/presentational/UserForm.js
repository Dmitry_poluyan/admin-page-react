import React, {PropTypes} from 'react';

import InputElement from '../../common/presentational/InputElement';

const UserForm = ({user, onSubmit, onChange, onCancel, saving}) => {
    function updateData(event) {
        let field = event.target.name;
        user[field] = event.target.value;
        onChange(user);
        isValidForm();
    }

    function isValidForm() {
        let form = document.getElementById('userForm');
        let btnSubmit = document.getElementById('userBtnSubmit');
        btnSubmit.disabled = !form.checkValidity();
    }

    function submitForm(event) {
        event.preventDefault();
        onSubmit();
    }

    function goToBack() {
        window.history.back();
    }

    return (
        <form name="userForm" id="userForm" className="form-horizontal">
            <InputElement
                name={'userName'}
                type={'text'}
                placeholder={'User name'}
                label={'User name'}
                value={user.userName}
                onChange={updateData}
                maxlength={'20'}
                minlength={'4'}
                require={'true'}
                pattern={'^[a-zA-Z0-9.-]+$'}
            />

            <InputElement
                name={'firstName'}
                type={'text'}
                placeholder={'First name'}
                label={'First name'}
                value={user.firstName}
                onChange={updateData}
                maxlength={'20'}
                minlength={'2'}
                require={'true'}
                pattern={'[a-zA-Z\s-]+$'}
            />

            <InputElement
                name={'lastName'}
                type={'text'}
                placeholder={'Last name'}
                label={'Last name'}
                value={user.lastName}
                onChange={updateData}
                maxlength={'20'}
                minlength={'2'}
                require={'true'}
                pattern={'^[a-zA-Z]+$'}
            />

            <InputElement
                name={'email'}
                type={'email'}
                placeholder={'Email'}
                label={'Email'}
                value={user.email}
                onChange={updateData}
                maxlength={'30'}
                minlength={'6'}
                require={'true'}
            />

            <div className="btn-form-right">
                <input
                    id="userBtnSubmit"
                    type="submit"
                    disabled={saving}
                    value={saving ? 'Saving...' : 'Save'}
                    className="btn btn-primary btn-margin"
                    onClick={submitForm}
                />

                <a disabled={saving}
                   className="btn btn-primary"
                   onClick={onCancel ? onCancel : goToBack}
                >
                    Cancel
                </a>
            </div>
        </form>
    );
};

UserForm.propTypes = {
    user: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    saving: PropTypes.bool
};

export default UserForm;
