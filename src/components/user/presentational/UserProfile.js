import React, {PropTypes} from 'react';

const UserProfile = ({user}) => {
    return (
        <div>
            <div className="adm-profile">
                <div className="col-sm-3">
                    <img className="profile-img" src="../img/profile.png" alt="profile"/>
                </div>
                <div className="col-sm-9">
                    <h4>User profile</h4>
                    <div className="adm-profile-items clearfix">
                        <div className="col-sm-3"><p>User name:</p></div>
                        <div className="col-sm-9"><p>{user.userName}</p></div>
                        <div className="col-sm-3"><p>First name:</p></div>
                        <div className="col-sm-9"><p>{user.firstName}</p></div>
                        <div className="col-sm-3"><p>Last name:</p></div>
                        <div className="col-sm-9"><p>{user.lastName}</p></div>
                        <div className="col-sm-3"><p>Email:</p></div>
                        <div className="col-sm-9"><p>{user.email}</p></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

UserProfile.propTypes = {
    user: PropTypes.object.isRequired
};

export default UserProfile;
