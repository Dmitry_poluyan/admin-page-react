import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';

import UsersTable from '../presentational/users.table/UsersTable';
import SearchInput from '../../common/container/SearchInput';
import Paginations from '../../common/container/Paginations';
import * as userCtrl from '../../../controllers/userCtrl';

class UsersPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            filteredUsers: Object.assign([], props.users),
            searchString: ''
        };

        this.onDoSearch = this.onDoSearch.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentWillMount() {
        this.props.userCtrl.getUsersByPageNumber();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({filteredUsers: Object.assign([], nextProps.users)});
    }

    onDoSearch(searchString) {
        searchString.length > 0
            ? this.props.userCtrl.searchUsersByCharacters(searchString)
            : this.props.userCtrl.getUsersByPageNumber();

        this.setState({searchString: searchString});
    }

    onChangePage(numPage) {
        this.props.userCtrl.getUsersByPageNumber(numPage);
    }

    render() {
        let usersTable = this.state.filteredUsers.length > 0
            ? <UsersTable users={this.state.filteredUsers}/>
            : <h4>There are no users.</h4>;

        let paginations = this.props.totalCount > this.props.numOnPage && this.state.searchString.length === 0
            ? <Paginations onChangePage={this.onChangePage} totalCount={this.props.totalCount} numOnPage={this.props.numOnPage}/>
            : null;

        return (
            <div>
                <h3>All users
                    <Link className="btn-add" to="/admin/createUser">+</Link>
                </h3>
                <SearchInput doSearch={this.onDoSearch}/>
                {usersTable}
                {paginations}
            </div>
        );
    }
}

UsersPage.propTypes = {
    users: PropTypes.array.isRequired,
    totalCount: PropTypes.number.isRequired,
    numOnPage: PropTypes.number.isRequired,
    userCtrl: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        users: state.USER.users,
        totalCount: state.USER.pagination.totalCount,
        numOnPage: state.USER.pagination.numOnPage
    };
}

function mapDispatchToProps(dispatch) {
    return {
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
