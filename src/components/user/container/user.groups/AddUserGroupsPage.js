import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as groupCtrl from '../../../../controllers/groupCtrl';
import * as userCtrl from '../../../../controllers/userCtrl';
import AddGroupForUserForm from '../../presentational/AddGroupForUserForm';

class AddUserGroupsPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            group: {
                _id: ''
            },
            saving: false
        };

        this.onUpdateUserGroupState = this.onUpdateUserGroupState.bind(this);
        this.onAddGroupForUser = this.onAddGroupForUser.bind(this);
    }

    componentWillMount() {
        this.userId = this.props.params.id;
    }

    componentDidMount() {
        this.props.groupCtrl.loadGroups();
    }

    onUpdateUserGroupState(group) {
        return this.setState({group: group});
    }

    onAddGroupForUser() {
        this.setState({saving: true});

        this.props
            .userCtrl
            .addGroupForUser(this.state.group._id, this.userId)
            .then(() => this.redirect())
            .catch(error => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        let userId = this.userId;
        toastr.success('Group added');
        this.setState({saving: false});
        this.context.router.push('/admin/users/details/' + userId + '/groups/list/');
    }

    render() {
        return (
            <div className="container-form margin-form">
                <h3>Add group for user</h3>
                <AddGroupForUserForm
                    group={this.state.group}
                    groups={this.props.groups}
                    onSubmit={this.onAddGroupForUser}
                    onChange={this.onUpdateUserGroupState}
                    saving={this.state.saving}
                />
            </div>
        );
    }
}

AddUserGroupsPage.propTypes = {
    params: PropTypes.object.isRequired,
    groupCtrl: PropTypes.object.isRequired,
    userCtrl: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired
};

AddUserGroupsPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        groups: state.GROUP.groups
    };
}

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch),
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUserGroupsPage);
