import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as groupCtrl from '../../../../controllers/groupCtrl';
import * as userCtrl from '../../../../controllers/userCtrl';
import GroupsTable from '../../../group/presentational/groups.table/GroupsTable';

class UserGroupsListPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.onDeleteGroup = this.onDeleteGroup.bind(this);
    }

    componentWillMount() {
        this.userId = this.props.params.id;
    }

    componentDidMount() {
        this.props.groupCtrl.getUserGroupsByUserId(this.userId);
    }

    onDeleteGroup(groupId) {
        let userId = this.userId;

        if (groupId && userId && confirm('Are you sure?')) {
            this.props
                .userCtrl
                .deleteGroupFromUser(groupId, userId)
                .then(() => toastr.success('Group updated'))
                .catch(error => toastr.error(error));
        }
    }

    render() {
        let groupsTable = this.props.groups.length > 0
            ? <GroupsTable groups={this.props.groups} onDelete={this.onDeleteGroup}/>
            : <h4>There are no groups.</h4>;

        return (
            <div>
                {groupsTable}
            </div>
        );
    }
}

UserGroupsListPage.propTypes = {
    params: PropTypes.object.isRequired,
    groupCtrl: PropTypes.object.isRequired,
    userCtrl: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        groups: state.GROUP.userGroups
    };
}

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch),
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserGroupsListPage);
