import React, {PropTypes, Component} from 'react';
import {Link, IndexLink} from 'react-router';

class UserGroupsPage extends Component {
    render() {
        let id = this.props.params.id;

        return (
            <div>
                <ul className="nav nav-pills adm-navbar">
                    <li>
                        <IndexLink to={'admin/users/details/' + id + '/groups/list'} activeClassName="active">Groups list</IndexLink>
                    </li>
                    <li>
                        <Link to={'admin/users/details/' + id + '/groups/add'} activeClassName="active">Add group for user</Link>
                    </li>
                </ul>
                {this.props.children}
            </div>
        );
    }
}

UserGroupsPage.propTypes = {
    children: PropTypes.object,
    params: PropTypes.object.isRequired
};

export default UserGroupsPage;
