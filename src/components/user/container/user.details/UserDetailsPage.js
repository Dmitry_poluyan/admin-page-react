import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as userCtrl from '../../../../controllers/userCtrl';
import UserNavbar from '../../presentational/UserNavbar';

class UserDetailsPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.onDeleteUser = this.onDeleteUser.bind(this);
    }

    componentWillMount() {
        this.userId = this.props.params.id;
    }

    componentDidMount() {
        this.props.userCtrl.getUserById(this.userId);
    }

    onDeleteUser() {
        if (this.userId && confirm('Are you sure?')) {
            this.props
                .userCtrl
                .deleteUserById(this.userId)
                .then(() => this.redirect())
                .catch(error => toastr.error(error));
        }
    }

    redirect() {
        toastr.success('User deleted');
        this.context.router.push('/admin/users');
    }

    render() {
        let userId = this.userId;

        return (
            <div className="row">
                <div className="col-sm-9 border-right animate-content-user">
                    {this.props.children}
                </div>
                <div className="col-sm-3">
                    <UserNavbar id={userId} onDelete={this.onDeleteUser}/>
                </div>
            </div>
        );
    }
}

UserDetailsPage.propTypes = {
    children: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    userCtrl: PropTypes.object.isRequired
};

UserDetailsPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(UserDetailsPage);
