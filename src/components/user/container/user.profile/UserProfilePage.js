import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';

import UserProfile from '../../presentational/UserProfile';

class UserProfilePage extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <UserProfile user={this.props.user}/>
        );
    }
}

UserProfilePage.propTypes = {
    user: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.USER.currentUser
    };
}

export default connect(mapStateToProps)(UserProfilePage);
