import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as userCtrl from '../../../../controllers/userCtrl';
import UserForm from '../../presentational/UserForm';


class UpdateUserPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            user: Object.assign({}, props.user),
            saving: false
        };

        this.onUpdateUserState = this.onUpdateUserState.bind(this);
        this.onUpdateUser = this.onUpdateUser.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user._id !== nextProps.user._id) {
            this.setState({user: Object.assign({}, nextProps.user)});
        }
    }

    onUpdateUserState(user) {
        this.setState({user: user});
    }

    onUpdateUser() {
        this.setState({saving: true});

        this.props
            .userCtrl
            .updateUser(this.state.user)
            .then(() => this.redirect())
            .catch(error => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        toastr.success('User updated');
        this.setState({saving: false});
        this.context.router.push('/admin/users/details/' + this.state.user._id + '/profile/');
    }

    render() {
        return (
            <div className="container-form margin-form">
                <h3>Edit user</h3>
                <UserForm
                    onChange={this.onUpdateUserState}
                    onSubmit={this.onUpdateUser}
                    user={this.state.user}
                    saving={this.state.saving}
                />
            </div>

        );
    }
}

UpdateUserPage.propTypes = {
    userCtrl: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

UpdateUserPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.USER.currentUser
    };
}

function mapDispatchToProps(dispatch) {
    return {
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateUserPage);
