import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as userCtrl from '../../../../controllers/userCtrl';
import UserForm from '../../presentational/UserForm';


class CreateUserPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            user: {
                userName: '',
                firstName: '',
                lastName: '',
                email: ''
            },
            saving: false
        };

        this.onUpdateUserState = this.onUpdateUserState.bind(this);
        this.onSaveUser = this.onSaveUser.bind(this);
    }

    onUpdateUserState(user) {
        this.setState({user: user});
    }

    onSaveUser() {
        this.setState({saving: true});

        this.props
            .userCtrl
            .createUser(this.state.user)
            .then(() => this.redirect())
            .catch(error => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        toastr.success('User saved');
        this.setState({saving: false});
        this.context.router.push('/admin/users');
    }

    render() {
        return (
            <div className="container-form margin-form">
                <h3>Add new user</h3>
                <UserForm
                    onChange={this.onUpdateUserState}
                    onSubmit={this.onSaveUser}
                    user={this.state.user}
                    saving={this.state.saving}
                />
            </div>
        );
    }
}

CreateUserPage.propTypes = {
    userCtrl: PropTypes.object.isRequired
};

CreateUserPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(CreateUserPage);
