import React, {PropTypes, Component} from 'react';

class App extends Component {
    render() {
        return (
            <div className="container">
                <div className="block-container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.object.isRequired
};

export default App;
