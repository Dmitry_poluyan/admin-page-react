import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';

import Navbar from '../../common/presentational/Navbar';

class AdminPage extends Component {
    render() {
        return (
            <div>
                <Navbar loading={this.props.loading}/>
                {this.props.children}
            </div>
        );
    }
}

AdminPage.propTypes = {
    children: PropTypes.object,
    loading: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
    return {
        loading: state.ajaxCallsInProgress > 0
    };
}

export default connect(mapStateToProps)(AdminPage);
