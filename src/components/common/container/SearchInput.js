import React, {PropTypes, Component} from 'react';

class SearchInput extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            searchString: ''
        };

        this.onChangeSearchInput = this.onChangeSearchInput.bind(this);
        this.onClearSearchInput = this.onClearSearchInput.bind(this);
    }

    onChangeSearchInput(e) {
        var query = e.target.value.trim();
        this.setState({searchString: query});
        this.props.doSearch(query);
    }

    onClearSearchInput() {
        this.setState({searchString: ''});
        this.props.doSearch('');
    }

    render() {
        let clearSearchInputBtn = this.state.searchString.length > 0
            ? <div className="input-group-addon"><a onClick={this.onClearSearchInput}>X</a></div>
            : null;

        return (
            <div className="form-group">
                <div className="input-group">
                    <div className="input-group-addon">Search</div>
                    <input type="text"
                           name="search"
                           id="search"
                           className="form-control"
                           value={this.state.searchString}
                           onChange={this.onChangeSearchInput}
                           placeholder="Type here"/>
                    {clearSearchInputBtn}
                </div>
            </div>
        );
    }
}

SearchInput.propTypes = {
    doSearch: PropTypes.func.isRequired
};

export default SearchInput;
