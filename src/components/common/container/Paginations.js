import React, {Component, PropTypes} from 'react';
import {Pagination} from 'react-bootstrap';

class Paginations extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            activePage: 1
        };
        this.onSelectPage = this.onSelectPage.bind(this);
    }

    onSelectPage(numPage) {
        this.setState({activePage: numPage});
        this.props.onChangePage(numPage);
    }

    render() {
        return (
            <Pagination
                prev
                next
                first
                last
                ellipsis
                items={Math.ceil(this.props.totalCount / this.props.numOnPage)}
                activePage={this.state.activePage}
                onSelect={this.onSelectPage}
            />
        );
    }
}

Paginations.propTypes = {
    onChangePage: PropTypes.func.isRequired,
    totalCount: PropTypes.number.isRequired,
    numOnPage: PropTypes.number.isRequired
};

export default Paginations;
