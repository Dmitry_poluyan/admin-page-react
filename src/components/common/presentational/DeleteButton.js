import React, {PropTypes} from 'react';

const DeleteButton = ({onDelete}) => {
    return (
        <a className="delete-icon" onClick={onDelete}>X</a>
    );
};

DeleteButton.propTypes = {
    onDelete: PropTypes.func.isRequired
};

export default DeleteButton;
