import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';

import LoadingDots from './LoadingDots';

const Navbar = ({loading}) => {
    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <span className="navbar-brand header-page">Admin page</span>
                    <IndexLink
                        className="navbar-brand"
                        to="/admin/users"
                        activeClassName="active">
                        Users
                    </IndexLink>
                    <Link
                        className="navbar-brand"
                        to="/admin/groups"
                        activeClassName="active">
                        Groups
                    </Link>
                    {loading && <LoadingDots interval={10} dots={9}/>}
                </div>
            </div>
        </nav>
    );
};

Navbar.propTypes = {
    loading: PropTypes.bool.isRequired
};

export default Navbar;
