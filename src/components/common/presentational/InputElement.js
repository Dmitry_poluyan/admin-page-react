import React, {PropTypes} from 'react';

const InputElement = ({name, label, onChange, placeholder, value, error, pattern, maxlength, minlength, require, type}) => {
    let wrapperClass = 'form-group';
    if (error && error.length > 0) {
        wrapperClass += ' ' + 'has-error';
    }

    return (
        <div className={wrapperClass}>
            <label className="col-sm-3 control-label" htmlFor={name}>{label}</label>
            <div className="col-sm-9">
                <input
                    type={type}
                    name={name}
                    id={name}
                    className="form-control"
                    placeholder={placeholder}
                    value={value}
                    required={require}
                    maxLength={maxlength}
                    minLength={minlength}
                    pattern = {pattern}
                    onChange={onChange}
                />
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

InputElement.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string.isRequired,
    pattern: PropTypes.string,
    maxlength: PropTypes.string,
    minlength: PropTypes.string,
    require: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string
};

export default InputElement;
