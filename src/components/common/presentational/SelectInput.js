import React, {PropTypes} from 'react';

const SelectInput = ({name, label, onChange, defaultOption, value, error, options, keyKey, keyValue, keyText}) => {
    let optionItems = options.map((option) => <option key={option[keyKey]} value={option[keyValue]}>{option[keyText]}</option>);

    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <div className="field">
                <select name={name}
                        value={value}
                        onChange={onChange}
                        className="form-control"
                >
                    <option value="">{defaultOption}</option>
                    {optionItems}
                </select>
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    );
};

SelectInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    defaultOption: PropTypes.string,
    value: PropTypes.string.isRequired,
    keyKey: PropTypes.string.isRequired,
    keyValue: PropTypes.string.isRequired,
    keyText: PropTypes.string.isRequired,
    error: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.object)
};

export default SelectInput;
