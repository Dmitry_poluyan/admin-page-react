import React from 'react';
import {Link} from 'react-router';

const NotFoundPage = () => {
    return (
        <div className="col-lg-offset-1">
            <h1>Page not found! </h1>
            <Link to="/">
                <h1>на главную?</h1>
            </Link>
        </div>
    );
};

export default NotFoundPage;
