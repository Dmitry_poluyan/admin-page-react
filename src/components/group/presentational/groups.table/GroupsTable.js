import React, {PropTypes} from 'react';
import GroupTableRow from './GroupTableRow';

const GroupsTable = ({groups, onDelete}) => {
    let groupsRows = groups.map(group =><GroupTableRow key={group._id} onDelete={onDelete} group={group}/>);

    return (
        <div>
            <table className="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>Group name</th>
                    <th>Group title</th>
                    {onDelete ? <th>Delete</th> : null}
                </tr>
                </thead>
                <tbody>
                {groupsRows}
                </tbody>
            </table>
        </div>
    );
};

GroupsTable.propTypes = {
    groups: PropTypes.array.isRequired,
    onDelete: PropTypes.func
};

export default GroupsTable;
