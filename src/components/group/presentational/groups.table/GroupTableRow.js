import React, {PropTypes} from 'react';
import {Link} from 'react-router';

import DeleteButton from '../../../common/presentational/DeleteButton';

const GroupTableRow = ({group, onDelete}) => {
    let onDeleteContainer = onDelete ? onDelete.bind(null, group._id) : null;
    let deleteRow = onDelete ? <td><DeleteButton onDelete={onDeleteContainer}/></td> : null;

    return (
        <tr>
            <td>
                <Link to={'/admin/groups/details/' + group._id}>{group.groupName}</Link>
            </td>
            <td>{group.groupTitle}</td>
            {deleteRow}
        </tr>
    );
};

GroupTableRow.propTypes = {
    group: PropTypes.object.isRequired,
    onDelete: PropTypes.func
};

export default GroupTableRow;
