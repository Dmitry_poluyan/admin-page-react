import React, {PropTypes} from 'react';

const UserProfile = ({group}) => {
    return (
        <div>
            <div className="adm-profile">
                <div className="col-sm-3">
                    <img className="profile-img" src="../img/Group_ava.png" alt="profile"/>
                </div>
                <div className="col-sm-9">
                    <h4>Group profile</h4>
                    <div className="adm-profile-items clearfix">
                        <div className="col-sm-3"><p>Group name:</p></div>
                        <div className="col-sm-9"><p>{group.groupName}</p></div>
                        <div className="col-sm-3"><p>Group title:</p></div>
                        <div className="col-sm-9"><p>{group.groupTitle}</p></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

UserProfile.propTypes = {
    group: PropTypes.object.isRequired
};

export default UserProfile;
