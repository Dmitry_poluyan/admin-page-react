import React, {PropTypes} from 'react';
import InputElement from '../../common/presentational/InputElement';

const GroupForm = ({group, onSubmit, onChange, onCancel, saving}) => {
    function updateData(event) {
        let field = event.target.name;
        group[field] = event.target.value;
        onChange(group);
        isValidForm();
    }

    function isValidForm() {
        let form = document.getElementById('groupForm');
        document.getElementById('groupBtnSubmit').disabled = !form.checkValidity();
    }

    function submitForm(event) {
        event.preventDefault();
        onSubmit();
    }

    function goToBack() {
        window.history.back();
    }

    return (
        <form id="groupForm" className="form-horizontal">
            <InputElement
                type={'text'}
                name="groupName"
                label="Group name"
                placeholder="Group name"
                value={group.groupName}
                onChange={updateData}
                maxlength={'20'}
                minlength={'6'}
                require={'true'}
                pattern={'^[a-zA-Z0-9.-]+$'}
            />

            <InputElement
                type={'text'}
                name="groupTitle"
                label="Group title"
                placeholder="Group title"
                value={group.groupTitle}
                onChange={updateData}
                maxlength={'20'}
                minlength={'4'}
                require={'true'}
            />

            <div className="btn-form-right">
                <input
                    type="submit"
                    disabled={saving}
                    id="groupBtnSubmit"
                    value={saving ? 'Saving...' : 'Save'}
                    className="btn btn-primary btn-margin"
                    onClick={submitForm}
                />

                <a disabled={saving}
                   className="btn btn-primary"
                   onClick={onCancel ? onCancel : goToBack}
                >
                    Cancel
                </a>
            </div>
        </form>
    );
};

GroupForm.propTypes = {
    group: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    onChange: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object
};

export default GroupForm;
