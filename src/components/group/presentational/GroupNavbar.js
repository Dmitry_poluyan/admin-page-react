import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';

const GroupNavbar = ({id, onDelete}) => {
    return (
        <ul className="nav nav-pills nav-stacked adm-navbar">
            <li>
                <IndexLink to={'admin/groups/details/' + id + '/profile'} activeClassName="active">Profile group</IndexLink>
            </li>
            <li>
                <Link to={'admin/groups/details/' + id + '/edit'} activeClassName="active">Edit group</Link>
            </li>
            <li>
                <Link to={'admin/groups/details/' + id + '/users'} activeClassName="active">Groups group</Link>
            </li>
            <li>
                <a onClick={onDelete}>Delete group</a>
            </li>
        </ul>
    );
};

GroupNavbar.propTypes = {
    id: PropTypes.string.isRequired,
    onDelete: PropTypes.func.isRequired
};

export default GroupNavbar;
