import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';

import GroupsTable from '../presentational/groups.table/GroupsTable';
import SearchInput from '../../common/container/SearchInput';
import * as groupCtrl from '../../../controllers/groupCtrl';

class GroupsPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            filteredGroups: Object.assign([], props.groups)
        };

        this.onDoSearch = this.onDoSearch.bind(this);
    }

    componentWillMount() {
        this.props.groupCtrl.loadGroups();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({filteredGroups: Object.assign([], nextProps.groups)});
    }

    onDoSearch(queryText) {
        let groups = Object.assign([], this.props.groups);
        let searchString = queryText.toLowerCase();

        searchString.length > 0
            ? groups = groups.filter((item) => item.groupName.toLowerCase().match(searchString) || item.groupTitle.toLowerCase().match(searchString))
            : null;

        this.setState({filteredGroups: groups});
    }

    render() {
        let groupsTable = this.state.filteredGroups.length > 0
            ? <GroupsTable groups={this.state.filteredGroups}/>
            : <h4>There are no groups.</h4>;

        return (
            <div>
                <h3>All groups
                    <Link className="btn-add" to="/admin/createGroup">+</Link>
                </h3>
                <SearchInput doSearch={this.onDoSearch}/>
                {groupsTable}
            </div>
        );
    }
}

GroupsPage.propTypes = {
    groups: PropTypes.array.isRequired,
    groupCtrl: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        groups: state.GROUP.groups
    };
}

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupsPage);
