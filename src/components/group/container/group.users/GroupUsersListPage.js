import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as userCtrl from '../../../../controllers/userCtrl';
import UsersTable from '../../../user/presentational/users.table/UsersTable';

class GroupUsersListPage extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        this.groupId = this.props.params.id;
    }

    componentDidMount() {
        this.props.userCtrl.getGroupUsersByGroupId(this.groupId);
    }

    render() {
        let usersTable = this.props.users.length > 0
            ? <UsersTable users={this.props.users}/>
            : <h4>There are no users.</h4>;

        return (
            <div>
                {usersTable}
            </div>
        );
    }
}

GroupUsersListPage.propTypes = {
    params: PropTypes.object.isRequired,
    userCtrl: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        users: state.USER.groupUsers
    };
}

function mapDispatchToProps(dispatch) {
    return {
        userCtrl: bindActionCreators(userCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupUsersListPage);
