import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';

import GroupProfile from '../../presentational/GroupProfile';

class GroupProfilePage extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <GroupProfile group={this.props.group}/>
        );
    }
}

GroupProfilePage.propTypes = {
    group: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        group: state.GROUP.currentGroup
    };
}


export default connect(mapStateToProps)(GroupProfilePage);
