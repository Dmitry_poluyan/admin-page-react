import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as groupCtrl from '../../../../controllers/groupCtrl';
import GroupForm from '../../presentational/GroupForm';


class CreateGroupPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            group: {
                groupName: '',
                groupTitle: ''
            },
            saving: false
        };

        this.onUpdateGroupState = this.onUpdateGroupState.bind(this);
        this.onSaveGroup = this.onSaveGroup.bind(this);
    }

    onUpdateGroupState(group) {
        return this.setState({group: group});
    }

    onSaveGroup() {
        this.setState({saving: true});

        this.props
            .groupCtrl
            .createGroup(this.state.group)
            .then(() => this.redirect())
            .catch(error => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        toastr.success('Group saved');
        this.setState({saving: false});
        this.context.router.push('/admin/groups');
    }

    render() {
        return (
            <div className="container-form margin-form">
                <h3>Add new group</h3>
                <GroupForm
                    onChange={this.onUpdateGroupState}
                    onSubmit={this.onSaveGroup}
                    group={this.state.group}
                    saving={this.state.saving}
                />
            </div>
        );
    }
}

CreateGroupPage.propTypes = {
    groupCtrl: PropTypes.object.isRequired
};

CreateGroupPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(CreateGroupPage);
