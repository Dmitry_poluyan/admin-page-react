import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as groupCtrl from '../../../../controllers/groupCtrl';
import GroupForm from '../../presentational/GroupForm';


class UpdateGroupPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            group: Object.assign({}, props.group),
            saving: false
        };

        this.onUpdateGroupState = this.onUpdateGroupState.bind(this);
        this.onUpdateGroup = this.onUpdateGroup.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.group._id !== nextProps.group._id) {
            this.setState({group: Object.assign({}, nextProps.group)});
        }
    }

    onUpdateGroupState(group) {
        return this.setState({group: group});
    }

    onUpdateGroup(event) {
        this.setState({saving: true});

        this.props
            .groupCtrl
            .updateGroup(this.state.group)
            .then(() => this.redirect())
            .catch(error => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        toastr.success('Group updated');
        this.setState({saving: false});
        this.context.router.push('/admin/groups/details/' + this.state.group._id + '/profile/');
    }

    render() {
        return (
            <div className="container-form margin-form">
                <h3>Edit group</h3>
                <GroupForm
                    onChange={this.onUpdateGroupState}
                    onSubmit={this.onUpdateGroup}
                    group={this.state.group}
                    saving={this.state.saving}
                />
            </div>

        );
    }
}

UpdateGroupPage.propTypes = {
    groupCtrl: PropTypes.object.isRequired,
    group: PropTypes.object.isRequired
};

UpdateGroupPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        group: state.GROUP.currentGroup
    };
}

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateGroupPage);
