import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as groupCtrl from '../../../../controllers/groupCtrl';
import GroupNavbar from '../../presentational/GroupNavbar';

class GroupDetailsPage extends Component {
    constructor(props, context) {
        super(props, context);

        this.onDeleteGroup = this.onDeleteGroup.bind(this);
    }

    componentWillMount() {
        this.groupId = this.props.params.id;
    }

    componentDidMount() {
        this.props.groupCtrl.getGroupById(this.groupId);
    }

    onDeleteGroup() {
        if (this.groupId && confirm('Are you sure?')) {
            this.props
                .groupCtrl
                .deleteGroupById(this.groupId)
                .then(() => this.redirect())
                .catch(error => toastr.error(error));
        }
    }

    redirect() {
        toastr.success('Group deleted');
        this.context.router.push('/admin/groups');
    }

    render() {
        let groupId = this.groupId;

        return (
            <div className="row">
                <div className="col-sm-9 border-right animate-content-group">
                    {this.props.children}
                </div>
                <div className="col-sm-3">
                    <GroupNavbar id={groupId} onDelete={this.onDeleteGroup}/>
                </div>
            </div>
        );
    }
}

GroupDetailsPage.propTypes = {
    children: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    groupCtrl: PropTypes.object.isRequired
};

GroupDetailsPage.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
    return {
        groupCtrl: bindActionCreators(groupCtrl, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(GroupDetailsPage);
