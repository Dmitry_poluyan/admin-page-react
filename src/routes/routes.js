import React from 'react';
import {Route, Redirect} from 'react-router';

import App from '../components/App';

import AdminPage from '../components/admin/container/AdminPage';

import NotFoundPage from '../components/common/presentational/notFoundPage';

import UsersPage from '../components/user/container/UsersPage';
import CreateUserPage from '../components/user/container/user.form/CreateUserPage';
import UserDetailsPage from '../components/user/container/user.details/UserDetailsPage';
import UserProfilePage from '../components/user/container/user.profile/UserProfilePage';
import UpdateUserPage from '../components/user/container/user.form/UpdateUserPage';
import UserGroupsPage from '../components/user/container/user.groups/UserGroupsPage';
import UserGroupsListPage from '../components/user/container/user.groups/UserGroupsListPage';
import AddUserGroupsPage from '../components/user/container/user.groups/AddUserGroupsPage';

import GroupsPage from '../components/group/container/GroupsPage';
import CreateGroupPage from '../components/group/container/group.form/CreateGroupPage';
import GroupDetailsPage from '../components/group/container/group.details/GroupDetailsPage';
import UpdateGroupPage from '../components/group/container/group.form/UpdateGroupPage';
import GroupProfilePage from '../components/group/container/group.profile/GroupProfilePage';
import GroupUsersListPage from '../components/group/container/group.users/GroupUsersListPage';

export default (
    <div>
        <Redirect from="/" to="/admin"/>
        <Route path="/" component={App}>

            <Redirect from="/admin" to="/admin/users"/>
            <Route path="/admin" component={AdminPage}>

                <Route path="/admin/users" component={UsersPage}/>

                <Redirect from="/admin/users/details/:id" to="/admin/users/details/:id/profile" />
                <Route path="/admin/users/details/:id" component={UserDetailsPage}>
                    <Route path="/admin/users/details/:id/profile" component={UserProfilePage}/>

                    <Route path="/admin/users/details/:id/edit" component={UpdateUserPage}/>

                    <Redirect from="/admin/users/details/:id/groups" to="/admin/users/details/:id/groups/list" />
                    <Route path="/admin/users/details/:id/groups" component={UserGroupsPage}>
                        <Route path="/admin/users/details/:id/groups/list" component={UserGroupsListPage}/>
                        <Route path="/admin/users/details/:id/groups/add" component={AddUserGroupsPage}/>
                    </Route>
                </Route>

                <Redirect from="/admin/groups/details/:id" to="/admin/groups/details/:id/profile"/>
                <Route path="/admin/groups/details/:id" component={GroupDetailsPage}>
                    <Route path="/admin/groups/details/:id/profile" component={GroupProfilePage}/>

                    <Route path="/admin/groups/details/:id/edit" component={UpdateGroupPage}/>

                    <Route path="/admin/groups/details/:id/users" component={GroupUsersListPage}/>
                </Route>

                <Route path="/admin/groups" component={GroupsPage}/>

                <Route path="/admin/createUser" component={CreateUserPage}/>

                <Route path="/admin/createGroup" component={CreateGroupPage}/>
            </Route>

            <Route path="*" component={NotFoundPage}/>
        </Route>
    </div>
);
